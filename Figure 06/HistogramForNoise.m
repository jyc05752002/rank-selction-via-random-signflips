n = 500;
p = 300;
rng(1);
Time_Covariance = diag([0.1*ones(1,n/2) 0.9*ones(1,n/2)]);
X = sqrt(Time_Covariance)*randn(n,p)/sqrt(n);
X_perm = zeros(n,p);
for j=1:p
    pe = randperm(n);
    X_perm(:,j) = X(pe,j);
end
X_signflip = (binornd(1,0.5,n,p)*2-1).*X;
s1 = svd(X);
s2 = svd(X_perm);
s3 = svd(X_signflip);

t1 = p/n*[0.1;0.9]; %location of population eigenvalues: H = 1/n\sum delta_{t_i}
t2 = p/n*[0.5];
gamma = n/p; 
[grid1, density1] =  spectrode(t1, gamma); %compute limit spectrum
grid1 = sqrt(grid1);
density1 = 2*grid1.*density1;
area1 = sum(diff(grid1).*density1(2:end));
[grid2, density2] =  spectrode(t2, gamma);
grid2 = sqrt(grid2);
density2 = 2*grid2.*density2;
area2 = sum(diff(grid2).*density2(2:end));
edges = 0:0.03:1.5;

hold on
h1 = histogram(s1, edges, 'Normalization','pdf');
%h2 = histogram(s2, edges, 'Normalization','pdf');
h3 = histogram(s3, edges, 'Normalization','pdf');
plot(grid1, density1/area1,'r','LineWidth',3) %plot
%plot(grid2, density2/area2,'b','LineWidth',3) %plot
%h = legend('Empirical spectrum of N','Empirical spectrum of N_{\pi}','Limiting spectrum of N','Limiting spectrum of N_{\pi}','Location','northeast');
h = legend('Empirical spectrum of N','Empirical spectrum of R \circ N','Limiting spectrum of N and R \circ N','Location','northeast');
set(h,'FontSize',16);
hold off
xlabel('Singular Value','FontSize',16);
ylabel('Density','FontSize',16);
