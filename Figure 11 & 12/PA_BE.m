function [num_sel,thresh] = PA_BE(X,p_thresh,num_perm)
% Estimate number of factors by Buja& Eyuboglu's Parallel Analysis

[n,p] = size(X);
s = svd(X);
X_perm= zeros(n,p);
%get eigenvalues of permutations
s_perm = zeros(min(n,p),num_perm);
rng(2);
for i=1:num_perm
    for j=1:p
        pe = randperm(n);
        X_perm(:,j) = X(pe,j);
    end
    s_perm(:,i) = svd(X_perm);
end

factor_retained=1;
num_factor=0;

while (factor_retained==1)&&(num_factor<min(n,p))
    num_factor= num_factor+1;
    %threshold is empirical percentile of permutation distribution
    thresh = prctile(s_perm(num_factor,:),p_thresh);
    %if data singular value is less than the threshold, don't keep it
    if s(num_factor)<=thresh
        factor_retained=0;
        num_factor= num_factor-1; %decrease by 1 to go back to the pre-error state
    end
end
num_sel  = num_factor;
