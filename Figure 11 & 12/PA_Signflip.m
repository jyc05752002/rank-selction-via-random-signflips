function [num_sel,thresh] = PA_Signflip(X,p_thresh,num_signflip)
% Estimate number of factors by Signflip Parallel Analysis

[n,p] = size(X);
s = svd(X);
%get eigenvalues of signflip
s_signflip = zeros(min(n,p),num_signflip);
rng(2);
for i=1:num_signflip
    X_signflip = (binornd(1,0.5,n,p)*2-1).*X;
    s_signflip(:,i) = svd(X_signflip);
end

factor_retained=1;
num_factor=0;

while (factor_retained==1)&&(num_factor<min(n,p))
    num_factor= num_factor+1;
    %threshold is empirical percentile of permutation distribution
    thresh = prctile(s_signflip(num_factor,:),p_thresh);
    %if data singular value is less than the threshold, don't keep it
    if s(num_factor)<=thresh
        factor_retained=0;
        num_factor= num_factor-1; %decrease by 1 to go back to the pre-error state
    end
end
num_sel  = num_factor;