function [num_sel,thresh] = PA_Signflip_plus(X,p_thresh,num_signflip)
% Estimate number of factors by Signflip PA+

[n,p] = size(X);
s = svd(X);
%get eigenvalues of signflip
s_signflip = zeros(1,num_signflip);
rng(2);
for i=1:num_signflip
    X_signflip = (binornd(1,0.5,n,p)*2-1).*X;
    s_signflip(:,i) = svds(X_signflip,1);
end

factor_retained=1;
num_factor=0;
thresh = prctile(s_signflip, p_thresh);

while (factor_retained==1)&&(num_factor<min(n,p))
    num_factor= num_factor+1;
    if s(num_factor)<=thresh
        factor_retained=0;
        num_factor= num_factor-1; %decrease by 1 to go back to the pre-error state
    end
end
num_sel  = num_factor;
