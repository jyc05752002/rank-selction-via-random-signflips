function [num_sel,thresh] = PA_plus(X,p_thresh,num_pa_plus)
% Estimate number of factors by PA+

[n,p] = size(X);
s = svd(X);
%get eigenvalues of signflip
s_pa_plus = zeros(1,num_pa_plus);
X_perm= zeros(n,p);
rng(2);
for i=1:num_pa_plus
    for j=1:p
        pe = randperm(n);
        X_perm(:,j) = X(pe,j);
    end
    s_pa_plus(i) = svds(X_perm,1);
end

factor_retained=1;
num_factor=0;
thresh = prctile(s_pa_plus, p_thresh);

while (factor_retained==1)&&(num_factor<min(n,p))
    num_factor= num_factor+1;
    if s(num_factor)^2<=thresh^2
        factor_retained=0;
        num_factor= num_factor-1; %decrease by 1 to go back to the pre-error state
    end
end
num_sel  = num_factor;