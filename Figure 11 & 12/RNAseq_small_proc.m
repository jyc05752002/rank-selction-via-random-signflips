%X is nxp - 10000x5000
%n=10000 cells
%p=5000 genes
load('dropSeq_5k_10k.mat')
X = dropSeq;
[n,p]=size(X);
X = X-sum(X)/n;
weight = sqrt(sum(X.^2));
X = X./(ones(n,1)*weight);
X(isinf(X)|isnan(X)) = 0;
%% All 4 methods
[num_sel_pa,thresh_pa] = PA_BE(X,100,20); %PA
[num_sel_pa_signflip,thresh_pa_signflip] = PA_Signflip(X,100,20); %Signflip PA
[num_sel_pa_plus,thresh_pa_plus] = PA_plus(X,100,20); %PA+
[num_sel_signflip_plus,thresh_signflip_plus] = PA_Signflip_plus(X,100,20); %Signflip PA+
%
s = svd(X);
%%
rng(2);
savefigs=1;
a = {'-','--','-.',':'};
figure, hold on
histogram(s(1:4889),2*floor(sqrt(p)));
xlim([min(s(s>0)), max(s)]);

pa=thresh_pa; %your point goes here
x=[pa,pa];
y=get(gca,'Ylim');
h1 = plot(x,y,'linewidth',2,'color',rand(1,3));
set(h1,'LineStyle',a{1});

pa=thresh_pa_signflip; %your point goes here
x=[pa,pa];
y=get(gca,'Ylim');
h2 = plot(x,y,'linewidth',2,'color',rand(1,3));
set(h2,'LineStyle',a{2});

pa=thresh_pa_plus; %your point goes here
x=[pa,pa];
y=get(gca,'Ylim');
h3 = plot(x,y,'linewidth',2,'color',rand(1,3));
set(h3,'LineStyle',a{3});

pa=thresh_signflip_plus; %your point goes here
x=[pa,pa];
y=get(gca,'Ylim');
h4 = plot(x,y,'linewidth',2,'color',rand(1,3));
set(h4,'LineStyle',a{4});
%pa=s(num_sel_pa_signflip); %your point goes here

legend([h1,h2,h3,h4],{'Permutation PA','Signflip PA','Upper-edge Permutation PA','Upper-edge Signflip PA'},'location','northeast','FontSize',15)
xlabel('Singular value')
ylabel('Number of singular values')
if savefigs==1
    filename = sprintf( './scRNAseq-hist-all.png');
    saveas(gcf, filename,'png');
    fprintf(['Saved Results to ' filename '\n']);
    %close(gcf)
end

%% scatterplots
tic;
[U,s,V] = svd(X);
toc;
s =diag(s);
%% these show the clustering of the samples
k = 12;
savefigs =1;
for i=1:k
    figure, scatter(U(:,2*i-1),U(:,2*i),'filled');
    xlabel(2*i-1);
    ylabel(2*i);
    set(gca,'xtick',[])
    set(gca,'xticklabel',[])
    set(gca,'ytick',[])
    set(gca,'yticklabel',[])
    if savefigs==1
        filename = sprintf( './scRNAseq-scatter-i=%d.png',i);
        saveas(gcf, filename,'png');
        fprintf(['Saved Results to ' filename '\n']);
        close(gcf)
    end
    set(gca,'fontsize',20)
end
