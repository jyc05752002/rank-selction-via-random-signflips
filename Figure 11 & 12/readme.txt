1. The raw data we use can be accessed at https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE63472,as GSE63472_P14Retina_merged_digital_expression.txt.gz. 

The paper reporting this data is 
"Highly Parallel Genome-wide Expression Profiling of Individual Cells Using Nanoliter Droplets." 
by Macosko EZ, Basu A, Satija R, Nemesh J et al., 
Cell. 2015 May 21;161(5):1202-1214. doi: 10.1016/j.cell.2015.05.002.,
https://pubmed.ncbi.nlm.nih.gov/26000488/.


2. We would like to thank Yuval Kluger and George Linderman for helping us with the data.


3. In the raw data, the 24,658 rows correspond to genes, and the 49,300 columns correspond to cells.  In the analysis of Macosko  et al. 2015, the matrix is pre-processed by the following steps:

1) Divide by total number of UMIs per cell (the sum of the columns), 
2) multiply by 10E3, 
3) apply log (ie log(1+transcripts-per-10,000)), because there are zeros).

George Linderman also generated several subsets of the raw data, such as dropSeq_5k_10k.mat, Retina_DropSeq_1E3_Random.csv,and Retina_DropSeq_1E3_Random_Preprocessed.csv. We currently do not have the full code for generating these subsets.


4. We use dropSeq_5k_10k.mat to test our methods. 


5. RNAseq_small_proc.m is the main file. 