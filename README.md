# Selecting the number of components in PCA via random signflips

Repository containing code associated with the paper

> David Hong, Yue Sheng, Edgar Dobriban. "Selecting the number of components in PCA via random signflips", 2020. https://arxiv.org/abs/2012.02985.

## Jupyter notebooks

Figures 1-4 and 7-10 have associated Jupyter notebooks.
Outputs were saved in the notebook so they can be seen without re-running.
For some of the notebooks,
results from long-running simulations
were cached using [CacheVariables.jl](https://github.com/dahong67/CacheVariables.jl).

| Filename          | Associated figure in paper | Julia version used | Cache folder |
| ----------------- | -------------------------- | ------------------ | ------------ |
| `Figure 01.ipynb` | Fig.  1                    | Julia 1.4.1        |              |
| `Figure 02.ipynb` | Fig.  2                    | Julia 1.4.1        |              |
| `Figure 03.ipynb` | Fig.  3                    | Julia 1.4.1        |              |
| `Figure 04.ipynb` | Fig.  4                    | Julia 1.4.1        |              |
| `Figure 07.ipynb` | Fig.  7                    | Julia 1.3.1        | `Figure 07/` |
| `Figure 08.ipynb` | Fig.  8                    | Julia 1.3.1        | `Figure 08/` |
| `Figure 09.ipynb` | Fig.  9                    | Julia 1.3.1        | `Figure 09/` |
| `Figure 10.ipynb` | Fig. 10                    | Julia 1.5.2        |              |

The files `JuliaManifest.toml` and `JuliaProject.toml`
define the Julia environment.
For more info see: https://pkgdocs.julialang.org/v1/environments/#Using-someone-else's-project

The file `matplotlibrc` provides configuration for `matplotlib` which is used for some of the plots.

## Matlab codes

Figures 6 and 11-12 were created in Matlab.
Associated codes are in the folders `Figure 06/` and `Figure 11 & 12/`.